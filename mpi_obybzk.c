/* Bajusz Istv�n OBYBZK P�rhuzamos algoritmusok beadand�.
A feladatom: Hat�rozza meg N eg�sz eset�n azok szorzat�t rekurz�van.
MPI szabv�ny, C nyelv, az input 2 hatv�nya legyen, malloc f�ggv�ny haszn�lata.
!!!!!!! El�fordulhat, hogy a program ind�t�sakor nem jelenik meg az n-t bek�r� sz�veg.
Ilyen esetben ugyan �gy megadhatja az n-t, a program m�k�dni fog.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"

void feloszt(int array[], int r, int a, int b, int rank, int elements_per_process, int size);

int a2[1000];

int main(int argc, char* argv[]) {
	int rank; // A processz rangja
	int numtasks; // A processzek sz�ma
	int elements_per_process; // Egy processzre jut� sz�mok sz�ma
	int n_elements_recieved; // Ennyi sz�mot fogad a f�processzt�l a feloszt�s sor�n a processz
	int n; // A sz�mokat t�rol� t�mb m�rete
	int eredmeny = 1; // A v�geredm�nyt t�rol� v�ltoz�, kezdetben 1, mivel szorz�sr�l van sz�

	//MPI inicializ�l�s
	int rc = MPI_Init(&argc, &argv);
	if (rc != MPI_SUCCESS) {
		printf("Hiba az MPI program inditasakor. Kilepes. \n");
		MPI_Abort(MPI_COMM_WORLD, rc);
	}

	MPI_Status status;

	MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (rank == 0) {
		printf("Adja meg az n-t! A tomb elemeinek szama 2^n lesz. n = \n");
		scanf_s("%d", &n);
		int m = n;
		n = 1;

		double start_time = MPI_Wtime();

		//B�rmilyen sz�m megad�sa eset�n biztos, hogy 2 hatv�nya lesz.
		for (int i = 0; i < m; i++) {
			n = n * 2;
		}

		elements_per_process = n / numtasks;

		//A sz�mokat t�rol� t�mb sz�m�ra helyfoglal�s
		int* array;
		array = (int*)malloc(sizeof(int) * n);

		for (int i = 0; i < n; i++) {
			array[i] = rand() % (10 + 1 - 1) + 1; //Random sz�mok 1 �s 10 k�z�tt
			printf("%d  ", array[i]);
		}
		printf("\n");

		//Megvizsg�lja, hogy 1-n�l t�bb processz fut-e
		if (numtasks > 1) {
			int rank = 0;
			int r = numtasks - 1;
			int size = n;
			feloszt(array, r, 0, n - 1, rank, elements_per_process, size);
		}

		//A f�processz ki�rja, hogy mely sz�mokat kapta meg
		printf("A %d processz a kovetkezo szamokat kapta: \n", rank);
		for (int i = 0; i < elements_per_process; i++) {
			printf("%d, ", array[i]);
		}
		printf("\n");

		//A f� processz hozz�szorozza az eredm�ny�hez az � r�szeredm�ny�t
		for (int i = 0; i < elements_per_process; i++) {
			eredmeny = eredmeny * array[i];
		}

		printf("A %d processz reszeredmenye: %d\n", rank, eredmeny);

		//�sszegy�jti a r�szeredm�nyeket a t�bbi processzt�l
		int tmp;
		for (int i = 1; i < numtasks; i++) {
			MPI_Recv(&tmp, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
			int sender = status.MPI_SOURCE;

			eredmeny = eredmeny * tmp;
		}
		double end_time = MPI_Wtime();
		//Ki�rja a v�geredm�nyt
		printf("!!!!!!!!! A vegeredmeny: %d !!!!!!!!!\n", eredmeny);
		printf("Idoigeny: %f", end_time - start_time);

		free(array);
	}
	//Slave processzek
	else {
		int size;
		MPI_Recv(&n_elements_recieved, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
		MPI_Recv(&a2, n_elements_recieved, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
		MPI_Recv(&size, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);

		elements_per_process = size / numtasks;

		//A processz ki�rja, hogy mely sz�mokat kapta meg
		printf("A %d processz a kovetkezo szamokat kapta meg: \n", rank);
		for (int i = 0; i < elements_per_process; i++) {
			printf("%d, ", a2[i]);
		}
		printf("\n");

		//R�szeredm�ny
		int reszeredmeny = 1;
		for (int i = 0; i < elements_per_process; i++) {
			reszeredmeny = reszeredmeny * a2[i];
		}

		printf("%d processz reszeredmenye: %d\n", rank, reszeredmeny);

		//Elk�ldi a gy�k�rprocessznek a r�szeredm�nyt
		MPI_Send(&reszeredmeny, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
	}

	MPI_Finalize();

	return 0;
}

/*A feloszt f�ggv�ny az intervallumot felezi. A k�t fel� osztott intervallum mindk�t fel�t tov�bbosztja,
addig, am�g sz�ks�ges m�g. Azt, hogy sz�ks�ges-e m�g tov�bb felezni, a m�lys�gi v�ltoz� (r) seg�ts�g�vel �llap�tja meg.
Ennek a v�ltoz�nak az �rt�ke azt jel�li, hogy az adott �gon mennyi sz�toszt�sra van m�g sz�ks�g. Ha r = 0,
akkor nem lesz t�bb oszt�s. A kett�osztott intervallum els� fel�t a processz megtartja, � fog vele sz�molni,
a m�sik fel�t tov�bbk�ldi. Azt, hogy melyik processznek kell �tk�ldenie, azt a m�lys�gi v�ltoz� akkori �rt�k�nek
seg�ts�g�vel sz�molja ki, �gy elker�li, hogy kimaradna valamely processz, vagy valamely processznek t�bbsz�r k�ldeni,
vagy esetleg olyan processznek akarnak k�ldeni, amely nem is l�tezik. A felosz�s ut�n minden processz kisz�molja
a saj�t intervallum�t �s tov�bb�tja a f�processznek, hogy az kisz�molja a v�geredm�nyt.*/

void feloszt(int array[], int r, int a, int b, int rank, int elements_per_process, int size) {
	if (r != 0) {
		r = (r - 1) / 2;
		int n = ((a + b) + 1) / 2;

		feloszt(array, r, a, n - 1, rank, elements_per_process, size);

		a = n;
		rank = rank + (r + 1);

		printf("Atkuldve %d processznek %d kezodintervallumtol. Ez a processz a %d indextol a %d index-el bezarolag fog szamolni. \n", rank, a, a, a + elements_per_process - 1);


		//�tk�ldi a megfelel� processznek
		MPI_Send(&elements_per_process, 1, MPI_INT, rank, 0, MPI_COMM_WORLD);
		MPI_Send(&array[a], elements_per_process, MPI_INT, rank, 0, MPI_COMM_WORLD);
		MPI_Send(&size, 1, MPI_INT, rank, 0, MPI_COMM_WORLD);

		feloszt(array, r, n, b, rank, elements_per_process, size);
	}
	else {
		return;
	}
}